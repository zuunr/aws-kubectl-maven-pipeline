# aws-kubectl-maven-pipeline

This module contains a Dockerfile to create a docker image based on the maven image, with the additional components aws-cli and kubectl installed.

It is intended to be used in Bitbucket/Gitlab pipelines to activate aws and kubectl functionality.

## Supported tags

* [`1.0.0`, (*1.0.0/Dockerfile*)](https://bitbucket.org/zuunr/aws-kubectl-maven-pipeline/src/1.0.0/Dockerfile)

## Usage

To use the image in a Bitbucket pipeline environment and authenticate with your aws environment, use the following yaml configuration as a base:

```yaml
- step:
    name: My kubectl step
    # Check the image version tag
    image: zuunr/aws-kubectl-maven-pipeline:1.0.0
    script:
      # Set $AWS_ACCESS_KEY_ID and $AWS_SECRET_ACCESS_KEY to the contents of your aws service account in the Bitbucket pipelines environment variables
      # Replace <MY_REGION> with your actual region
      # Replace <MY_KUBE_CLUSTER> with your actual cluster name
      - aws eks --region <MY_REGION> update-kubeconfig --name <MY_KUBE_CLUSTER>
      - kubectl get pods
```
